define({
	name: 'Boyce Avenue',

	background: {
		color: '#000000',
		url: '/tests/mock/images/preview.jpg',
		left: '50%',
		top: '50%',
		fixed: true
	},

	title: {
		enable: true,
		align: 'center',
		font: 'Marvel',
		color: '#ffffff',
		size: 30
	},

	player: {
		enable: true,
		style: 'bp2',
		swatch: '#ff0000',

		track_font: 'Marvel',
		track_color: '#fff',
		track_size: 30
	},

	itunes: {
		enable: true,
		url: 'http://'
	},

	social: {
		facebook: {
			id: 'BoyceAvenue'
		},
		twitter: {
			id: 'BoyceAvenue'
		},
		spotify: {
			id: '7CQwac16i1W5ej8YpuL3dv'
		}
	}
});
