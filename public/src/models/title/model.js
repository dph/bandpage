define([
	'underscore', 'backbone',
	'text!models/title/tmpl.css'
], function(_, Backbone, csstmpl) {
	return Backbone.Model.extend({
		css: function() { return _.template(csstmpl, this.toJSON()); },
		href: function() {
			return 'http://fonts.googleapis.com/css?family='+this.get('font')+':';
		},
		webfont: function() {
			return '<link href="'+this.href()+'" rel="stylesheet" type="text/css">';
		}
	});
});

