define([
	'underscore', 'backbone',
	'models/title/model',
	'models/background/model',
	'models/itunes/model',
	'models/player/model',
	'models/social/model',
	'text!models/page/tmpl.html'
], function(
	_, Backbone,
	Title,
	Background,
	ITunes,
	Player,
	Social,
	tmpl
) {
	return Backbone.Model.extend({
		initialize: function() {
			this.set('background', new Background(this.get('background')));
			this.set('title', new Title(this.get('title')));
			this.set('itunes', new ITunes(this.get('itunes')));
			this.set('player', new Player(this.get('player')));
			this.set('social', new Social(this.get('social')));
		},

		html: function() {
			return _.template(tmpl, this.data());
		},

		data: function() {
			return {
				name: this.get('name'),
				title: this.get('title').toJSON(),
				background: this.get('background').toJSON(),
				player: this.get('player').toJSON(),
				social: this.get('social').toJSON(),
				itunes: this.get('itunes').toJSON()
			};
		}
	});
});
