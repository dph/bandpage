define([
	'underscore', 'backbone',
	'text!models/social/tmpl.css'
], function(_, Backbone, csstmpl) {
	return Backbone.Model.extend({
		css: function() { return _.template(csstmpl, this.toJSON()); }
	});
});


