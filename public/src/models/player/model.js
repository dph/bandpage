define([
	'underscore', 'backbone',
	'text!models/player/tmpl.css'
], function(_, Backbone, csstmpl) {
	return Backbone.Model.extend({
		css: function() { return _.template(csstmpl, this.toJSON()); }
	});
});

