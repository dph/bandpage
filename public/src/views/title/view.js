define([
	'views/base/Group',
	'text!views/title/tmpl.html',
	'views/base/Text'
], function(GroupView, tmpl, TextEditor) {
	return new (GroupView.extend({
		tmplOpts: {
			id: 'title',
			heading: 'Group/Artist Name',
			toggle: true
		},

		init: function(page) {
			this.page = page;
			var model = this.model = page.get('title');
			var textEditor = this.textEditor = new TextEditor({
				color: this.model.get('color')
			});
			textEditor.on('change', function(info) { model.set(info.slug, info.value); });

			this.model.on('change', this.update, this);
			this.model.on('change:color', this.updateColor, this);
		},

		update: function() {
			var title = this.model;
			this.frameView.appendStyle('title', title.css());
			this.frameView.appendNode(title.cid+'_webfont', title.webfont());
		},

		updateColor: function() {
			if(this.$color[0]) this.$color.css('background-color', this.model.get('color'));
		},
		
		render: function() {
			var displayData = _.extend(this.model.toJSON(), {name: this.page.get('name')});
			this.$inner.html(_.template(tmpl, displayData));
			this.$('#title-text-editor').append(this.textEditor.el);

			this.$color = this.$('.txt-color');
			this.toggle(this.model.get('enable'));

			this.update();
			this.updateColor();

			return this;
		}
	}))();
});

