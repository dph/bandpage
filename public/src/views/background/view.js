define([
	'views/base/Group',
	'views/background/modal',
	'text!views/background/tmpl.html'
], function(GroupView, bgmodal, tmpl) {
	return new (GroupView.extend({
		tmplOpts: {
			id: 'background',
			heading: 'Page Background',
			toggle: false
		},

		events: _.extend({}, GroupView.prototype.events, {
			'click .change_bg': 'showBgModal'
		}),

		showBgModal: function() { bgmodal.show(); },

		init: function(page) {
			this.page = page;
			this.model = page.get('background');
			this.model.on('change', this.update, this);
		},

		update: function() {
			this.frameView.appendStyle('background', this.model.css());
			this.$preview.css('background-image', 'url('+this.model.get('url')+')');
		},
		
		render: function() {
			this.$inner.html(_.template(tmpl, {}));
			this.$preview = this.$('.preview');
			this.update();
			return this;
		}
	}))();
});

