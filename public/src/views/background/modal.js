define([
	'views/base/Modal',
	'text!views/background/modal.html'
], function(Modal, tmpl) {
	return new (Modal.extend({
		id: 'modal-background',
		initialize: function() {
			this.$el.appendTo('body').html(_.template(tmpl,{}));
		}
	}))();
});
