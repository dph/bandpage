define([
	'underscore', 'backbone',
	'text!views/base/Text.html',
	'idropper'
], function(_, Backbone, tmpl) {
	return Backbone.View.extend({
		tagName: 'ul',
		className: 'text-editor clearfix',
		events: {
			'dropdown:change .dropdown': 'dropdownChange',
			'change .title-input': 'keyChange'
		},

		dropdownChange: function(e) {
			this.trigger('change', e);
		},

		keyChange: function(e) {
			var $in = $(e.currentTarget);
			this.trigger('change', {
				slug: $in.data('key'),
				value: $in.val()
			});
		},

		initialize: function(opts) {
			this.$el.html(_.template(tmpl, {}));
			this.$('.idropper-container').iDropper({
				color: opts.color,
				size: 155,
				drag: _.bind(function(color) {
					this.dropdownChange({
						slug: 'color',
						value: color.hex
					});
				}, this),
				alpha: false
			});
		}
	});
});


