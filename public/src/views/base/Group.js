define([
	'views/frame',
	'text!views/base/Group.html'
], function(frameView, tmpl) {
	return Backbone.View.extend({
		className: 'group clearfix',

		events: {
			'click .toggle': 'toggle'
		},

		tmplOpts: {},

		toggle: function(e) {
			var on;
			if(typeof e === 'boolean') {
				this.$toggle[on = e ? 'addClass' : 'removeClass']('on');
			} else {
				on = this.$toggle.toggleClass('on').hasClass('on');
			}
			this.$el[on ? 'removeClass' : 'addClass']('disabled');

			if(this.model) this.model.set('enable', on);

			this.trigger('toggle', on);
		},

		initialize: function() {
			var opts = this.tmplOpts;
			if(opts.id) this.$el.addClass('group-'+opts.id);

			this.frameView = frameView;
			this.$el.html(_.template(tmpl, opts));
			this.$toggle = this.$('.toggle');
			this.$inner = this.$('.inner');
		},

		init: function(page) {
			this.page = page;
			if(typeof this.onInit === 'function') this.onInit.apply(this, arguments);
		},

		updateFrame: function() {
			var title = this.model;
			frameView.appendStyle('title', title.css());
			frameView.appendNode(title.cid, title.html());
		}
	});
});


