define([
	'underscore', 'backbone'
], function(_, Backbone) {
	var draggable = new (Backbone.View.extend({
		el: 'body',
		events: {
			'mousedown .modal-header': 'dragStart',
			'mousemove': 'dragMove',
			'mouseup': 'dragEnd'
		},
		initialize: function() {
			this.$cover = $('<div id="modal-cover" class="hide"/>').appendTo(this.$el);
			this.$el.append(this.$cover);
		},
		dragMove: function(e) {
			if((d = this.drag)) {
				d.$el.css({left: d.rX + e.clientX, top: d.rY + e.clientY});
				e.preventDefault();
			}
		},
		dragStart: function(e) {
			var $el = $(e.currentTarget).closest('.modal');
			var offset = $el.offset();
			this.drag = {
				$el: $el,
				rX: offset.left - e.clientX + $el.width()/2,
				rY: offset.top - e.clientY
			};
		},
		dragEnd: function(e) { delete this.drag; }
	}))();

	return Backbone.View.extend({
		opts: { heading: '' },
		className: 'modal hide',
		events: {
			'click .close': 'hide'
		},
		constructor: function() {
			Backbone.View.prototype.constructor.apply(this, arguments);
		},
		show: function() {
			this.$el.removeClass('hide');
			draggable.$cover.removeClass('hide');
		},
		hide: function() {
			this.$el.addClass('hide');
			draggable.$cover.addClass('hide');
		}
	});
});


