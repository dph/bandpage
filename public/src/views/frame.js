define([
	'jquery', 'underscore', 'backbone', 'jplayer'
], function(
	$, _, Backbone
) {
	return new (Backbone.View.extend({
		initialize: function(opts) {
			this.iframe = opts.iframe;
			var win = this.window = this.iframe.contentWindow;
			var doc = this.document = this.window.document;

			this.setElement(doc);
			this.$head = this.$('head');
			this.$body = this.$('body');

			this.trigger('ready');

			this.$body.click(function() { window.focus(); });
			this.$body.on('mouseup', _.bind(this.mouseup, this));
			this.$body.on('mousemove', _.bind(this.mousemove, this));
			this.$body.on('mousedown', '.sp-pad', _.bind(this.mousedown, this));
			this.$body.on('mouseenter', '.sp-block', _.bind(this.mouseenter, this));
			this.$body.on('mouseleave', '.sp-block', _.bind(this.mouseleave, this));
		},

		mouseenter: function(e) {
			if(!this.dragInfo) $(e.currentTarget).addClass('active');
		},
		mouseleave: function(e) {
			if(!this.dragInfo) $(e.currentTarget).removeClass('active');
		},

		mouseup: function(e) {
			this.$body.removeClass('sp-dragging');
			this.dragInfo = null;
		},

		mousemove: function(e) {
			var d = this.dragInfo;
			if(d) {
				var val = e.clientY - d.y;
				var prop = (d.which === 'top' ? 'margin-' : 'padding-') + d.which;
				d.$block.css(prop, val > 0 ? val : 0);
			}
		},

		mousedown: function(e) {
			var $target = $(e.currentTarget);
			var $block = $target.parents('.sp-block');
			var which = $target.data('which');
			var prop = (which === 'top' ? 'margin-' : 'padding-') + which;
			this.dragInfo = {
				y: e.clientY - parseInt($block.css(prop), 10),
				which: $target.data('which'),
				$block: $block
			};
			this.$body.addClass('sp-dragging');
		},

		render: function(html) {
			this.$body.html(html);
			this.$body.find('.sp-block').append(
				'<div class="sp-controls sp-dragger"></div>',
				'<div class="sp-controls sp-pad sp-pad-top" data-which="top"></div>',
				'<div class="sp-controls sp-pad sp-pad-bottom" data-which="bottom"></div>'
			);
			this.trigger('render');
		},

		appendStyle: function(name, styleString) {
			var $block = this.$head.find('.'+name);
			if(!$block.length) $block = $('<style/>').addClass(name).appendTo(this.$head);
			$block.html(styleString);
		},

		appendNode: function(id, str) {
			this.$('#'+id).remove();
			$(str).attr('id', id).prependTo(this.$body);
		}
	}))({ iframe: $('#frame_panel > iframe')[0] });
});
