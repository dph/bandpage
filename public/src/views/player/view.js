define([
	'views/base/Group',
	'text!views/player/tmpl.html',
	'views/base/Text'
], function(GroupView, tmpl, TextEditor) {
	return new (GroupView.extend({
		tmplOpts: {
			id: 'player',
			heading: 'Music Player',
			toggle: true
		},

		events: _.extend({}, GroupView.prototype.events, {
		}),

		init: function(page) {
			this.page = page;
			this.model = page.get('player');
			var textEditor = this.textEditor = new TextEditor({
				color: this.model.get('color')
			});

			this.model.on('change', this.update, this);
		},

		update: function() {
			var player = this.model;
			this.frameView.appendStyle('player', player.css());
		},

		render: function() {
			var tmplData = {
				styles: [
					{ id: 'bp1', label: 'Basic Player 1' },
					{ id: 'bp2', label: 'Basic Player 2' },
					{ id: 'bp3', label: 'Basic Player 3' }
				],
				current: this.model.get('style')
			};
			this.$inner.html(_.template(tmpl, tmplData));
			this.$('#track-name-editor').append(this.textEditor.el);

			this.toggle(this.model.get('enable'));
			this.update();
			return this;
		}
	}))();
});


