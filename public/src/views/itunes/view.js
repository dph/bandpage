define([
	'views/base/Group',
	'text!views/itunes/tmpl.html'
], function(GroupView, tmpl) {
	return new (GroupView.extend({
		tmplOpts: {
			id: 'itunes',
			heading: 'iTunes Button',
			toggle: true
		},

		events: _.extend({}, GroupView.prototype.events, {
		}),

		init: function(page) {
			this.page = page;
			this.model = page.get('itunes');
			this.model.on('change', this.update, this);
		},

		update: function() {
			this.frameView.appendStyle('itunes', this.model.css());
		},
		
		render: function() {
			this.toggle(this.model.get('enable'));
			this.$inner.html(_.template(tmpl, {}));
			this.update();
			return this;
		}
	}))();
});

