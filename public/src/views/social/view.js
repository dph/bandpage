define([
	'views/base/Group',
	'text!views/social/tmpl.html'
], function(GroupView, tmpl) {
	return new (GroupView.extend({
		tmplOpts: {
			id: 'social',
			heading: 'Social Links',
			toggle: true
		},

		events: _.extend({}, GroupView.prototype.events, {
		}),

		init: function(page) {
			this.page = page;
			this.model = page.get('social');
			this.model.on('change', this.update, this);
		},

		update: function() {
			this.frameView.appendStyle('social', this.model.css());
		},
		
		render: function() {
			this.toggle(this.model.get('enable'));
			this.$inner.html(_.template(tmpl, {}));
			this.update();
			return this;
		}
	}))();
});


