define([
	'views/title/view',
	'views/background/view',
	'views/itunes/view',
	'views/player/view',
	'views/social/view',
	'text!views/editPanel/tmpl.html',
	'twitter_bootstrap'
], function(titleView, bgView, itunesView, playerView, socialView, tmpl) {
	return new (Backbone.View.extend({

		id: 'edit_panel',

		init: function(page) {
			$('#spinnbar').append(this.$el);
			this.page = page;

			titleView.init(page);
			bgView.init(page);
			itunesView.init(page);
			playerView.init(page);
			socialView.init(page);

			this.render();
		},

		render: function() {
			this.$el.html(_.template(tmpl,{}));
			this.$('.groups').append(
				bgView.render().el,
				titleView.render().el,
				playerView.render().el,
				itunesView.render().el,
				socialView.render().el
			);
		}
	}))();
});
