define([
	'views/frame',
	'views/editPanel/view',
	'models/page/Page',
	'../tests/mock/data',
	'jquery'
], function(frameView, editPanel, Page, baseData) {

	var dropdown = new (Backbone.View.extend({
		events: {
			'click .dropdown-cover': 'hideDropdown',
			'click .dropdown-trigger': 'showDropdown',
			'click .dropdown > li': 'triggerDropdown'
		},
		initialize: function() {
			this.$dropdown = $('<div class="dropdown-cover"/>').appendTo(this.$el);
		},
		triggerDropdown: function(e) {
			var $target = $(e.currentTarget);
			var event = new $.Event('dropdown:change');
			event.value = $target.data('value');
			event.slug = $target.parent().data('slug');

			$target.parents('.dropdown').trigger(event);
			this.hideDropdown();
		},
		showDropdown: function(e) {
			$(e.currentTarget).parents('.dropdown-wrap').addClass('active');
			this.$dropdown.addClass('active');
		},
		hideDropdown: function() {
			$('.dropdown-wrap').removeClass('active');
			this.$dropdown.removeClass('active');
		}
	}))({el: 'body'});



	var app = new (Backbone.View.extend({
		events: {
			'click #customize': 'toggle'
		},

		initialize: function() {
			this.start(baseData);
		},

		toggle: function() {
			var $body = this.$el;
			var collapsed = $body.hasClass('collapsed');
			var val = collapsed ? 285 : 35;
			if(collapsed) $body.removeClass('collapsed');

			this.$spinnbar = this.$spinnbar || this.$('#spinnbar');
			this.$framePanel = this.$framePanel || this.$('#frame_panel');
			this.$framePanel.animate({left: val}, 200);
			this.$spinnbar.animate({width: val}, 200, function() {
				if(!collapsed) $body.addClass('collapsed');
			});
		},

		start: function(data) {
			var page = this.page = new Page(data);
			this.render();
			editPanel.init(page);
		},

		render: function() {
			frameView.render(this.page.html());

			var i$ = frameView.window.$;
			i$('#sp-jplayer').jPlayer({
				swfPath: '/lib/jplayer',
				supplied: 'mp3',
				ready: function() {
					i$(this).jPlayer('setMedia', { mp3: '/tests/mock/sample.mp3' });
				}
			});
		}
	}))({el: 'body'});
	
});
