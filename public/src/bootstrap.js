require = {
	baseUrl: '/src',
	paths: {
		jquery: '/lib/jquery',
		text: '/lib/text',
		backbone: '/lib/backbone',
		underscore: '/lib/underscore',
		jplayer: '/lib/jplayer/jquery.jplayer.min',
		select2: '/lib/select2/select2.min',
		idropper: '/lib/iDropper/iDropper',
		twitter_bootstrap: 'ui/bootstrap/js/bootstrap.min'
	},
	priority: [
		'jquery',
		'select2',
		'idropper',
		'jplayer',
		'twitter_bootstrap',
		'underscore',
		'backbone',
		'text'
	]
};
