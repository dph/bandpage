var ctrl = {
	index: function(req, res) {
		res.render('app/index', { title: 'Musically' });
	},
	frame: function(req, res) {
		res.render('app/frame', { });
	}
};

exports.setup = function(app) {
	app.get('/app', ctrl.index);
	app.get('/app/frame', ctrl.frame);
};
